const {request,response} = require("express");
const { QueryTypes } = require('sequelize');
const sequelize = require("../Db/con");
const AccountModel = require("../Models/accounts");
const TransactionModel = require("../Models/transaccions");
const AccountInscriptionModel = require("../Models/account-inscriptions");


exports.MakeTransaccion = async(req=request,res=response)=>{


    let params = {
        transacionAmount:Number(req.body.transacionAmount),
        userId:req.user.userId,
        accounIntOriginId:req.body.accounIntOriginId,
        accountInDestinationId:req.body.accountInDestinationId,
        transactionDescription:req.body.transactionDescription,
    };
  
     let AcInscripcionOriginObject = await  AccountInscriptionModel.findByPk(params.accounIntOriginId);
     let AcInscripcionDestinationObject = await  AccountInscriptionModel.findByPk(params.accountInDestinationId);
      
    let AccountObjectOrigin = await AccountModel.findByPk(AcInscripcionOriginObject.accountsAccountId);
    let AccountObjectDest = await AccountModel.findByPk(AcInscripcionDestinationObject.accountsAccountId);
   

    if( AccountObjectOrigin.accountBalance >= params.transacionAmount ){

    
    let transaction = await sequelize.transaction();
       
       
      try{
       let transaccionResult = await  TransactionModel.create({
            transacionAmount:params.transacionAmount,
            usersId:params.userId,
            accounIntOriginId:params.accounIntOriginId,
            accountInDestinationId:params.accountInDestinationId,
            transactionDescription:params.transactionDescription

        },{transaction});
        console.log("Create transaction",transaccionResult);
        await AccountObjectOrigin.update({accountBalance:AccountObjectOrigin.accountBalance-params.transacionAmount}
            ,
        {transaction});
        await AccountObjectDest.update({accountBalance:AccountObjectDest.accountBalance+params.transacionAmount}
            ,
        {transaction});
        console.log("Pass test",transaccionResult);


        (await transaction).commit();
        res.json({
            value:transaccionResult,
            status:200,
            msg:"Transaccion realizada con exito",

        });
        

      }catch(e){

        (await transaction).rollback();
        res.status(400).json({
            status:400,
            msg:e.message
        });

      }



       

    }else{
        res.status(400).json({
            status:400,
            msg:"Fondos insuficientes para realizar la transaccion"
        });
    }


}

exports.TransaccionHistory = async function(req=request,res=response){


    let params = {
        userId:req.user.userId,
        accounInscriptionId:req.body.accounInscriptionId
    };

    let transactionSended = await TransactionModel.findAll({
        where:{
            accounIntOriginId:params.accounInscriptionId
        },
        order:[["transaccionId","DESC"]]
    });
    let transactionReceived = await TransactionModel.findAll({
        where:{
            accountInDestinationId:params.accounInscriptionId
        },
        order:[["transaccionId","DESC"]]
    });
    let transactionSendCopy =[];
    await Promise.all( transactionSended.map( async(ele,index)=>{

        let AccountIn = await AccountInscriptionModel.findOne(
            {
                where:{accountInscriptionId:ele.accountInDestinationId},
                attributes:["accountsAccountId","accountInscriptionNickName"]
            });
            ele = ele.toJSON();
            ele.accountNick = AccountIn.accountInscriptionNickName;
            ele.Account = await AccountModel.findByPk(AccountIn.accountsAccountId);  
           
            transactionSendCopy.push(ele);

    } ) );
 
    let transactionReceivedCopy = [];
    await Promise.all( transactionReceived.map( async(ele,index)=>{

        let AccountIn = await AccountInscriptionModel.findOne(
            {
                where:{accountInscriptionId:ele.accounIntOriginId},
                attributes:["accountsAccountId","accountInscriptionNickName"]
            });
            ele = ele.toJSON();
            ele.accountNick = AccountIn.accountInscriptionNickName;
            ele.Account = await AccountModel.findByPk(AccountIn.accountsAccountId);
            transactionReceivedCopy.push(ele);
            

    } ) );


   
    res.json({
        status:200,
        value:{
            transactionSendCopy,
            transactionReceivedCopy
        }

    });

}