const {request,response} = require("express");
const EntityModel = require("../Models/entities");


exports.GetList = async function(req=request,res=response){

  let entities = await  EntityModel.findAll();

  res.json({
      state:true,
      value:entities,
      msg:""
  });

};