const {request,response} = require("express");
const UserModel = require("../Models/users");
const {sha256} = require("js-sha256");
var jwt = require('jsonwebtoken');



exports.RegisterUser = async function(req=request,res=response){
    
    let UserObject = {
        userFirstName:req.body.userFirstName,
        userLastName:req.body.userLastName,
        userDocId:req.body.userDocId,
        userEmail:req.body.userEmail,
        userPassword:sha256(req.body.userPassword)
    };
  
    let user  =  await  UserModel.findOne({where:{userEmail:req.body.userEmail}}) ;

   if( user ){

    res.status(400).json({
        msg:"Ya existe un usuario con ese email",
        status:400,
    });
   }else{
        
    
    await UserModel.create(UserObject);
    
    res.json({
        msg:"El usuario ha sido creado con exito",
        status:200,
    });

   }

}

exports.LoginUser = async function( req=request , res=response ){



    let params = {
        userEmail:req.body. userEmail,
        userPassword:sha256(req.body.userPassword)
    };

   let UserEx = await UserModel.findOne({
        where:params
    });




    if( UserEx ){

       
  
        let TokenSesion = jwt.sign({
            userId:UserEx.userId,
            userFirstName:UserEx.userFirstName,
            userLastName:UserEx.userLastName,
            userEmail:UserEx.userEmail,
    
        },process.env.JSON_WEB_TOKEN_KEY);

        res.json({
            msg:"",
            value:{
                userFirstName:UserEx.userFirstName,
                userLastName:UserEx.userLastName,
                userEmail:UserEx.userEmail,
                tokenUser:TokenSesion
            },
        });

    }else{

        res.status(400).json({
            msg:"Usuario o contraseña incorrectos",
            status:400,
            value:null
        });



    }



}