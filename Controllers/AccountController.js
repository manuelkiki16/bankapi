const {request,response} = require("express");
const sequelize = require("../Db/con");
const EntityModel = require("../Models/entities");
const AccountModel = require("../Models/accounts");
const AccountInscriptionModel = require("../Models/account-inscriptions");



exports.CreateAccount = async function(req=request,res=response){


    let params = {
        accountNumber:req.body.accountNumber,
        accountType:req.body.accountType,
        accountStatus:req.body.accountStatus,
        AccountBalance:1000000,
        accountInscriptionNickName:req.body.accountInscriptionNickName,
        accountInscriptionDocId:req.body.accountInscriptionDocId,
        accountInscriptionType:req.body.accountInscriptionType,
        EntityId:req.body.EntityId,
    };

    let transaction =  await sequelize.transaction();
     try{

        let EntityObject = await EntityModel.findByPk(params.EntityId,{transaction});
        let AccounExist = await AccountModel.findOne({
            where:{accountNumber:params.accountNumber}
        });



        if( AccounExist ){
            return res.status(400).json({
                msg:"El numero de cuenta ya ha sido registrado",
                status:400
            })
        }
        console.log("Entity",EntityObject);
        let accountObject = await AccountModel.create({
            accountNumber:params.accountNumber,
            accountNickName:params.accountNickName,
            accountType:params.accountType,
            accountBalance:params.accountInscriptionType == 'SELF'?1000000:0,
            antitiesEntitiId:EntityObject.entitiId
        },{transaction});
       
     
       
        await AccountInscriptionModel.create({
            accountInscriptionNickName:params.accountInscriptionNickName,
            accountInscriptionDocId:params.accountInscriptionDocId,
            accountInscriptionType:params.accountInscriptionType,
            accountsAccountId:accountObject.accountId,
            usersUserId:req.user.userId

        },{transaction}); 

        transaction.commit();
        res.json({
            status:200,
            msg:"La cuenta ha sido vinculada con exito"
        });

     }catch(e){
         transaction.rollback();
         console.log("Error al inicializar la transaccion",e);
        return res.status(500).json({
             status:500,
             msg:e.message,

         });

     }
    


}
exports.JoinAccount =async function(req=request,res=response){

    let params = {
        accountNumber:req.body.accountNumber,
        accountType:req.body.accountType,
        accountStatus:req.body.accountStatus,
        accountInscriptionNickName:req.body.accountInscriptionNickName,
        accountInscriptionDocId:req.body.accountInscriptionDocId,
        accountInscriptionType:req.body.accountInscriptionType,
        antitiesEntitiId:req.body.EntityId,
    };

    let accountObject = await AccountModel.findOne({
        where:{
            accountNumber:params.accountNumber,
            accountType:params.accountType,
            antitiesEntitiId:params.antitiesEntitiId
        }});
    if( accountObject ){

        let  AcInscriptionObject = await AccountInscriptionModel.findOne({
            where:{
                accountsAccountId:accountObject.accountId,
                usersUserId:req.user.userId
            }
        });

        if( AcInscriptionObject ){

            return res.status(400).json({
                status:400,
                msg:"La cuenta ya esta registrada con otro alias "+AcInscriptionObject.accountInscriptionNickName
            });

        }else{
            
                AccountInscriptionModel.create({
                    accountInscriptionNickName:params.accountInscriptionNickName,
                    accountInscriptionDocId:params.accountInscriptionDocId,
                    accountInscriptionType:params.accountInscriptionType,
                    usersUserId:req.user.userId,
                    accountsAccountId:accountObject.accountId
                });

                return res.status(200).json({
                    status:200,
                    msg:"Su cuenta se ha vinculado con exito"
                });


        }



    }else{
        return res.status(400).json({
            status:400,
            msg:"La cuenta no existe en el sistema"
        });
    }    


}

exports.ChangeNickNameAcount = async function(req=request,res=response){

    let params = {
        accountInscriptionId:req.body.accountInscriptionId,
        accountInscriptionNickName:req.body.accountInscriptionNickName
    }
 
    try{
       await  AccountInscriptionModel.update({accountInscriptionNickName:params.accountInscriptionNickName},{
           where:{accountInscriptionId:params.accountInscriptionId,usersId:req.user.userId}
       });
       res.json({
           msg:"El alias de la cuenta ha sido cambiado"
       });

    }catch(e){
        res.status(400).json({
            msg:"No se encontro una cuenta asociada con estos datos",
            value:"",
            status:400
        });
    }


}


exports.ListarCuentas = async function(req=request,res=response){


    let acInscriptions = await AccountInscriptionModel.findAll({where:{usersUserId:req.user.userId,accountInscriptionType:req.body.accountInscriptionType}});
    let accountJoinedList = [];
    await  Promise.all( acInscriptions.map( async (ele,index)=>{

        let Account = await AccountModel.findOne({where:{accountId:ele.accountsAccountId}});  
        
        accountJoinedList.push({
            accountInscriptionId:ele.accountInscriptionId,
            accountInscriptionNickName:ele.accountInscriptionNickName,
            accountInscriptionDocId:ele.accountInscriptionDocId,
            Account

        });

    }));


    res.json({
        msg:"list",
        value:accountJoinedList,
        status:200
    });
        


}