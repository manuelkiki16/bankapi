# Api para banco

Banco app

### Stack Utilizado
[express.js](https://expressjs.com/es/)  
[sequalize ORM](https://sequelize.org/)  
[express validator](https://express-validator.github.io/docs/) 
[Patron MVC](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador)   
Base de datos
[Mysql ](https://www.mysql.com/)


### Pre-requisitos 📋

_Se necesita tener node.js de entorno para android


## Comenzando 🚀

**PASO 1 ** Clone el proyecto en gitlab con el siguiente comando

```
git  clone https://gitlab.com/manuelkiki16/bankapi
```
**PASO 2 ** Ubiquese en el directorio raiz y ejecute

```
npm install

```

**PASO 3 ** Cree un archivo ** .env ** en el directorio raiz y coloque las siguentes variables

```
APP_TEST=YES


DEPLOY_PORT=3500
JSON_WEB_TOKEN_KEY=ETO5959355N484

DB_PROD_HOST=192.168.64.2
DB_PROD_DBNAME=bancotest
DB_PROD_USER=root
DB_PROD_PASS=


DB_TEST_HOST=localhost
DB_TEST_DBNAME=bancotest
DB_TEST_USER=roottest
DB_TEST_PASS=

```

**PASO 4 ** Cree una base de datos llamada bancotest

```
CREATE SCHEMA bancotest

```
**PASO 5 ** Cambie las variables de entorno del archivo ** .env ** por su configuracion local

**PASO 6** Dirijase a la carpeta raiz del proyecto y ejecute el siguiente comando

```
npm start

```



### Eso es todo de momento



## Aqui tienes algunos datos que yo utilize para pruebas


CUENTAA TEST
PROPIA

CUENTA N 10439583824
TIPO AHORRO
Alias CUENTA TEST 1
DOC ID  204465242032
ENTIDAD: BA-BANCO AGRARIO


PROPIA

CUENTA N 22439583810
TIPO CORRIENTE
Alias CUENTA TEST 2
DOC ID  204465242032
ENTIDAD:  BC-BANCOLOMBIA


CUENTA TERCERO

USUARIO Tercero 1
EMAIL tercero@gmail.com
PASS 12345678

TERCERO

CUENTA N 22439583800
TIPO AHORRO
Alias CUENTA TERCERA 1
DOC ID  204465242000
ENTIDAD:  BC-BANCOLOMBIA

TERCERO

CUENTA N 22439583811
TIPO AHORRO
Alias CUENTA TERCERA 2
DOC ID  204465242000
ENTIDAD:  BC-BANCOLOMBIA