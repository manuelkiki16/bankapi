const { body,check,validationResult } = require('express-validator');



exports.createAccountValidator = async function(req, res, next){



    await check('accountNumber').notEmpty().isLength({max:11,min:11}).run(req);
    await check('accountType').notEmpty().toUpperCase().isIn(["AHORRO","CORRIENTE"]).run(req);
 
    
    await check('accountInscriptionNickName').notEmpty().run(req);
    await check('accountInscriptionDocId').notEmpty().isLength({min:12,max:12}).isInt().run(req);
    await check('accountInscriptionType').toUpperCase().isIn(["SELF"]).run(req);

    await check('EntityId').notEmpty().isInt().run(req);





    

    const result = validationResult(req);

    if (!result.isEmpty()) {

        return res.status(400).json({ erros: result.array(),
          msg:"Bad request fields",
        status:400,
      value:{} });

      }
      next();
  
   

}

exports.joinAccountOtherValidator = async function (req, res, next) {


  await check('accountNumber').notEmpty().isLength({max:11,min:11}).run(req);
  await check('accountType').notEmpty().toUpperCase().isIn(["AHORRO","CORRIENTE"]).run(req);

  
  await check('accountInscriptionNickName').notEmpty().run(req);
  await check('accountInscriptionDocId').notEmpty().isLength({min:12,max:12}).isInt().run(req);
  await check('accountInscriptionType').toUpperCase().isIn(["OTHER"]).run(req);

  await check('EntityId').notEmpty().isInt().run(req);

      const result = validationResult(req);

    if (!result.isEmpty()) {

        return res.status(400).json({ erros: result.array(),
          msg:"Bad request fields",
        status:400,
      value:{} });

      }
      next();
  
}

exports.ListarCuentasValidator = async function(req, res, next){

  await check('accountInscriptionType').toUpperCase().isIn(["SELF","OTHER"]).run(req);

  const result = validationResult(req);

  if (!result.isEmpty()) {

      return res.status(400).json({ erros: result.array(),
        msg:"Bad request fields",
      status:400,
    value:{} });

    }
    next();

}
exports.chageNickNameValidator = async function (req, res, next) {

  await check('accountInscriptionId').notEmpty().isInt().run(req);
  await check('accountInscriptionNickName').notEmpty().run(req);
 
  

  if (!result.isEmpty()) {

    return res.status(400).json({ erros: result.array(),
      msg:"Bad request fields",
    status:400,
  value:{} });

  }
  next();

}