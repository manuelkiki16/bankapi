const { body,check,validationResult } = require('express-validator');


exports.createUserValidator = async function(req, res, next){

    await check('userFirstName').notEmpty().run(req);
    //await check('userLastName').notEmpty().run(req);
    //await check('userDocId').isNumeric().optional({nullable:true,checkFalsy:true}).run(req);
    await check('userEmail').trim().notEmpty().isEmail().run(req);
    await check('userPassword').notEmpty().isLength({min:8}).run(req);
   

    const result = validationResult(req);
   
    if (!result.isEmpty()) {
      return res.status(400).json({ erros: result.array(),
        msg:"Bad request fields",
      status:400,
    value:{} });
    }
    next();

};

exports.LoginUserValidator = async function(req,res,next){

  await check('userEmail').trim().notEmpty().isEmail().run(req);
  await check('userPassword').notEmpty().run(req);
 

  const result = validationResult(req);
  if (!result.isEmpty()) {
    return res.status(400).json({ erros: result.array(),
      msg:"Bad request",
    status:400,
  value:{} });
  }
  next();


}