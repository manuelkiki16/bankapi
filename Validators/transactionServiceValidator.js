const { body,check,validationResult } = require('express-validator');


exports.MaketTransactonValidator = async function(req, res, next){

    await check('transacionAmount').notEmpty().isNumeric().run(req);
    await check('accounIntOriginId').notEmpty().isNumeric().run(req);
    await check('accountInDestinationId').notEmpty().isNumeric().run(req);
  
     const result = validationResult(req);

    if (!result.isEmpty()) {

        return res.status(400).json({ erros: result.array(),
          msg:"Bad request fields",
        status:400,
      value:{} });

      }
      next();



}
exports.TransactionHistoryValidator = async function(req,res,next){


    await check('accounInscriptionId').notEmpty().isNumeric().run(req);

    const result = validationResult(req);

    if (!result.isEmpty()) {

        return res.status(400).json({ erros: result.array(),
          msg:"Bad request fields",
        status:400,
      value:{} });

      }
      next();

}