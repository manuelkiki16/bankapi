const { request, response} = require("express");
const jwt = require('jsonwebtoken');

module.exports =  function (req=request, res=response, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1];
  
    if (token == null){ 
        return res.status(401).json({
        status:401,
         msg:"Autenticacion requerida"
    });

}
  
    jwt.verify(token, process.env.JSON_WEB_TOKEN_KEY ,{ignoreExpiration:true} ,(err, user) => {
      console.log(err)
  
      if (err) return res.status(403).json({
        status:403,
         msg:"Autenticacion no valida"
    });
  
      req.user = user
  
      next();
    })
  }