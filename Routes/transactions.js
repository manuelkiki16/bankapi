const express = require('express');
const {MakeTransaccion,TransaccionHistory} = require("../Controllers/TransaccionController");
const {MaketTransactonValidator,TransactionHistoryValidator} = require("../Validators/transactionServiceValidator");
const SesionMiddleware = require("../middlewares/sesionMidleware");



const transactionRoutes = express.Router();
transactionRoutes.use(SesionMiddleware);


transactionRoutes.post("/makeTransaccion",MaketTransactonValidator,MakeTransaccion);
transactionRoutes.post("/transactionHistory",TransactionHistoryValidator,TransaccionHistory);


module.exports = transactionRoutes;