const express = require('express');
const  {GetList} = require("../Controllers/EntitiesController");
const {RegisterUser,LoginUser} = require("../Controllers/UsersController");
const {createUserValidator,LoginUserValidator} = require("../Validators/UserServicevalidator");

const defaultRoute = express.Router();

defaultRoute.get("/entities",GetList);
defaultRoute.post("/createUser",createUserValidator,RegisterUser);
defaultRoute.post("/login",LoginUserValidator,LoginUser);



module.exports = defaultRoute;