const express = require('express');
const {CreateAccount,ListarCuentas,JoinAccount,ChangeNickNameAcount} = require("../Controllers/AccountController");
const {createAccountValidator,ListarCuentasValidator,joinAccountOtherValidator,chageNickNameValidator} = require("../Validators/AccountServiceValidator");
const SesionMiddleware = require("../middlewares/sesionMidleware");



const accountRoutes = express.Router();
accountRoutes.use(SesionMiddleware);


accountRoutes.post("/createAccount",createAccountValidator,CreateAccount);
accountRoutes.post("/joinAccount",joinAccountOtherValidator,JoinAccount);
accountRoutes.post("/listAccount",ListarCuentasValidator,ListarCuentas);
accountRoutes.post("/changeNickAccount",chageNickNameValidator,ChangeNickNameAcount);

module.exports = accountRoutes;