const userModel = require("../Models/users");
const accountModel = require("../Models/accounts");
const accountInscriptionModel = require("../Models/account-inscriptions");
const transaccionModel = require("../Models/transaccions");
const entitiesModel = require("../Models/entities");
const sequelize = require("../Db/con");

/**
 * Crear relacion entre tablas 
 */




exports.syncDataBase = async function(){
console.log("DB INIT SYNC");
await sequelize.sync();
InsertDefaultData();
console.log("DB FINISH SYNC");
}
/**
 * Method to insert any default data to storage like master tables
 * must be always findOrCreate to store default data
 */
 async function InsertDefaultData(){
    console.log("Insert Default DATA");
 await entitiesModel.findOrCreate(
     {where:{entitiName:"BANCOLOMBIA"},defaults:{ entitiName:"BANCOLOMBIA",entitiCode:"BC"}
 });

 await entitiesModel.findOrCreate(
    {where:{entitiName:"BANCO AGRARIO"},defaults:{ entitiName:"BANCO AGRARIO",entitiCode:"BA"}
});

await entitiesModel.findOrCreate(
    {where:{entitiName:"BANCO CAJA SOCIAL"},defaults:{ entitiName:"BANCO CAJA SOCIAL",entitiCode:"BCS"}
});



 }

