
const { Sequelize } = require('sequelize');



module.exports = new Sequelize(
    process.env.APP_TEST == 'YES'? process.env.DB_TEST_DBNAME:process.env.DB_PROD_DBNAME,
    process.env.APP_TEST == 'YES'? process.env.DB_TEST_USER:process.env.DB_PROD_USER,
    process.env.APP_TEST == 'YES'? process.env.DB_TEST_PASS:process.env.DB_PROD_PASS,
     {
    host: process.env.APP_TEST == 'YES'? process.env.DB_TEST_HOST:process.env.DB_PROD_HOST,
    dialect: "mysql"
  });