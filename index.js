require('dotenv').config();
const DbSyncTables = require("./Db/associations");
const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');
const app = express();

//Routers 
const defaultRouters = require("./Routes/default");
const AccountRoutes = require("./Routes/accounts");
const TransactionRoutes = require("./Routes/transactions");

DbSyncTables.syncDataBase();

app.use(cors());
app.use(express.json());

app.use(express.urlencoded({
    extended:true
}));
app.use(defaultRouters);
app.use("/accounts",AccountRoutes);
app.use("/transactions",TransactionRoutes);


  app.listen(process.env.DEPLOY_PORT, () => {
    console.log(`Example app listening at http://localhost:${process.env.DEPLOY_PORT}`)
  })