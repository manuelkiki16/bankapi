const { DataTypes } = require('sequelize');
const sequelize = require("../Db/con");


module.exports = sequelize.define('accountsInscription', {
    // Model attributes are defined here
    accountInscriptionId:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    accountInscriptionNickName: {
        type: DataTypes.STRING(200),
        allowNull: true
    },
    accountInscriptionDocId:{
        type:DataTypes.STRING(12),
        allowNull:true
    },
    accountInscriptionType: {
      type: DataTypes.STRING(200),
      allowNull: false,
      comment:"SELF,OTHER"
  },
  usersUserId:{
      type:DataTypes.INTEGER,
      allowNull:false,
      
  },
  accountsAccountId:{
      type:DataTypes.INTEGER,
      allowNull:false
  }
   
    

  }, {
      timestamps:true,
    tableName:"accounts-inscriptions",

  });
  