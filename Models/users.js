const { DataTypes } = require('sequelize');
const sequelize = require("../Db/con");


module.exports = sequelize.define('user', {
    // Model attributes are defined here
    userId:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    userFirstName: {
        type: DataTypes.STRING(200),
        allowNull: false
      },
    userLastName: {
      type: DataTypes.STRING(200)
      // allowNull defaults to true
    },
    userDocId: {
      type: DataTypes.STRING(12),
      allowNull:true
      // allowNull defaults to true
    },
    userEmail:{
        type: DataTypes.STRING(50),
        allowNull: false, 
        unique:true
    },
    userPassword:{
        type: DataTypes.STRING(250),
        allowNull: false, 
    },

  }, {
      timestamps:true,
    tableName:"users",

  });
  