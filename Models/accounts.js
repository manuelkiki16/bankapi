const { DataTypes } = require('sequelize');
const sequelize = require("../Db/con");



module.exports = sequelize.define('account', {
    // Model attributes are defined here
    accountId:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
   accountNumber: {
      type: DataTypes.STRING(11),
      allowNull:false
      // allowNull defaults to true
    },
    accountType:{
        type:DataTypes.STRING(20),
        allowNull:false,
        comment:"AHORRO,CORRIENTE"
    },
    accountStatus:{
        type:DataTypes.BOOLEAN,
        allowNull:true,
        defaultValue:true
    },
    accountBalance:{
        type:DataTypes.FLOAT,
        allowNull:true,
        defaultValue:0

    },
    antitiesEntitiId:{
        type:DataTypes.INTEGER,
        allowNull:false,
        
    },
    

  }, {
      timestamps:true,
    tableName:"accounts",

  });