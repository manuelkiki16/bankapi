const { DataTypes } = require('sequelize');
const sequelize = require("../Db/con");


module.exports = sequelize.define('entiti', {
    // Model attributes are defined here
    entitiId:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    entitiName: {
        type: DataTypes.STRING(200),
        allowNull: false
      },
    entitiCode: {
      type: DataTypes.STRING(20)
      // allowNull defaults to true
    },
    
    

  }, {
      timestamps:true,
    tableName:"entities",

  });