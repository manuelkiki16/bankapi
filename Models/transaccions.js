const { DataTypes } = require('sequelize');
const sequelize = require("../Db/con");


module.exports = sequelize.define('transaction', {
    // Model attributes are defined here
    transaccionId:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    transacionAmount: {
      type: DataTypes.FLOAT,
      allowNull:false,
      // allowNull defaults to true
    },
    usersId:{
      type:DataTypes.INTEGER,
      allowNull:false,
      
  },
   accounIntOriginId:{
    type:DataTypes.INTEGER,
    allowNull:false,
  },
   accountInDestinationId:{
    type:DataTypes.INTEGER,
    allowNull:false,
   },
   transactionDescription:{
      type:DataTypes.STRING(255),
      allowNull:true
   },



  },{
    timestamps:false,
    tableName:"transaccions",

  });
  